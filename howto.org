# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org
#+REVEAL_PREAMBLE: <div class="legalese"><p><a href="./imprint.html">Imprint</a> | <a href="./privacy.html">Privacy Policy</a></p></div>

#+TITLE: How to create presentations with emacs-reveal
#+AUTHOR: Jens Lechtenbörger
#+DATE: July 2018

* Presentation Hints
** General
   - This is a [[https://revealjs.com][reveal.js]] presentation
   - Key bindings and navigation
     - Press “?” to see key bindings of reveal.js
       - In general, “n” and “p” move to next and previous slide; mouse
	 wheel works as well
       - Search with Ctrl-Shift-F
     - Up/down (swiping, arrows) move within sections,
       left/right jump between sections (type “o” to see what is where)
     - Type slide’s number followed by Enter to jump to that slide
     - Browser history (buttons, Alt-CursorLeft, Alt-CursorRight)
     - Zoom with Ctrl-Mouse or Alt-Mouse

** Offline work
   - Students often ask for download-able presentations
   - Alternatives
     1. Clone repository, build presentations locally (see [[#usage][Usage]])
     2. Download build artifacts from
        [[https://gitlab.com/oer/emacs-reveal-howto/pipelines][recent pipeline]]
        (if not expired)
     3. Generate PDF
        - Why, really?
	  - Why not download source files instead?
	  - [[http://orgmode.org/][Org mode]], which is plain text
	- Change the URL by adding “?print-pdf” after “.html”,
	  then print to PDF file (usually, Ctrl-p)
	- Alternatively, generate PDF via LaTeX from org
	  source file

** Audio
   :PROPERTIES:
   :reveal_extra_attr: data-audio-src="./audio/Tours_-_01_-_Enthusiast.mp3"
   :END:
   - If audio is embedded, [[https://en.wikipedia.org/wiki/Ogg][the free Ogg format]] is used
     - Playback should start automatically
       - Currently, you should be hearing Enthusiast by
         [[http://freemusicarchive.org/music/Tours/][Tours]]
	 - Licensed under a
  	   [[http://creativecommons.org/licenses/by/3.0/][Creative Commons Attribution License]]
       - See [[https://github.com/rajgoel/reveal.js-plugins/tree/master/audio-slideshow#user-content-compatibility-and-known-issues][compatibility and known issues of the underlying audio plugin]]
       - [[https://www.mozilla.org/en-US/firefox/][Firefox]],
         which I recommend as browser in general
         ([[https://blogs.fsfe.org/jens.lechtenboerger/2015/06/09/three-steps-towards-more-privacy-on-the-net/][here in English]]
         and [[https://www.informationelle-selbstbestimmung-im-internet.de/][here in German]]),
         seems to work everywhere
     - Audio controls are shown at bottom left

** (Speaker) Notes
   - Slides contain additional notes as plain text if you see the
     folder icon at the top right (as on this slide)
     [[file:reveal.js/css/theme/folder_inbox.png]]
     - Click on that icon or press “s” to see the “speaker notes view”
     - You need to allow pop-ups
       - If the pop-up window does not work, you may need to press “s”
         twice or close the pop-up window once
#+BEGIN_NOTES
These are sample notes
- Lists can be used here
- You can time your presentation
  - Maybe look at [[https://gitlab.com/lechten/talks-2018/blob/master/2018-04-24-Blockchain.org][one]] of my talks to see how to define timing
#+END_NOTES

* Introduction

** What’s This?
   # Note the use of macro reveallicense() below to show an inline image
   # with vertically aligned, rotated license statement.
   # Alternatively, one could also use standard org mode features and
   # replace the macro with a #+CAPTION, which would be displayed
   # underneath the image.
   # Again alternatively, later as part of slide figure-with-meta-data,
   # you can see the use of macro revealimg(), to display a
   # horizontally centered image with caption and license statement.
   # The immediately following slide describes the required format of
   # meta-data.
   # Both macros are defined in emacs-reveal/config.org.
   - Emacs-reveal is [[https://fsfe.org/about/basics/freesoftware.en.html][free software]]
     to generate [[https://revealjs.com/][reveal.js]] presentations (slides with audio) from simple text
     files in [[http://orgmode.org/][Org mode]]
     {{{reveallicense("./figures/3d-man/board.meta","30vh")}}}
     - Benefits
     - For your audience
       - Self-contained presentations embedding audio
       - Usable on lots of (including mobile and offline) devices with
         just a browser
     - For you as producer
       - Separation of layout and contents
	 - Similarly to, e.g., LaTeX
       - Simple text format allows diff and merge for ease of collaboration

** Prerequisites
  - I suppose (and strongly recommend) that you use GNU/Linux
    ([[https://getgnulinux.org/switch_to_linux/try_or_install/][help on getting started]])
    - Actually, not much here is operating system specific
  - Emacs-reveal should really be used with the text editor [[https://www.gnu.org/software/emacs/][GNU Emacs]]
    - (You could try other editors and build presentations within
      GitLab, thanks to emacs-reveal’s GitLab infrastructure)
      - (In fact, you do not need an editor at all but could edit
        presentations using a Web browser on ~GitLab.com~, e.g., with the
        [[https://gitlab.com/-/ide/project/oer/emacs-reveal-howto/edit/master/][Web IDE]]
        (requires login))

** Installation
   1. Download software
      - Install emacs-reveal-howto
	- ~git clone https://gitlab.com/oer/emacs-reveal-howto.git~
	- ~cd emacs-reveal-howto/~
      - Install submodules
	- ~git submodule sync --recursive~
	- ~git submodule update --init --recursive~
   2. Install and configure Emacs packages
      - ~emacs --batch --load emacs-reveal/install.el --funcall install~
      - Add a line like this to =~/.emacs=
	- =(load "/path/to/emacs-reveal/reveal-config.el")=


* Usage
  :PROPERTIES:
  :CUSTOM_ID: usage
  :END:

** Alternatives
   1. Create presentations locally on Command Line
   2. Create presentations in Emacs
   3. Create and publish presentations on [[https://about.gitlab.com/][GitLab]]
      {{{reveallicense("./figures/logos/GitLab-wm_no_bg.meta","10vh")}}}
      - Based on underlying continuous integration infrastructure
        using [[https://www.docker.com/][Docker]] image
        {{{reveallicense("./figures/logos/docker-horizontal.meta","10vh")}}}

** Build Presentations on Command Line
   1. Create Org file in directory ~emacs-reveal-howto/~
      - See contained source file for this presentation, ~howto.org~
   2. Build presentations for files ending in ~.org~ (except internal
      ones listed in ~elisp/publish.el~)
      - ~emacs --batch --load elisp/publish.el --funcall org-publish-all~
      - Presentations are built in subdirectory ~public/~
   3. Open presentation in [[https://www.mozilla.org/en-US/firefox/][Firefox]]
      - E.g.: ~firefox public/howto.html~
   4. Optional: Copy ~public/~ to web server for public accessibility

** Building Presentations in Emacs
   1. Generate HTML presentation for visited
      ~.org~ file using the usual Org export functionality: @@html:<br>@@
      Press ~C-c C-e R B~
      - This generates an HTML file in the same directory and opens it
	in your default browser
      - For this to work, necessary resources, in particular the
	~reveal.js~ directory, must be accessible in the ~.org~ file’s
	directory

** Build Presentations on GitLab
   1. Fork [[https://gitlab.com/oer/emacs-reveal-howto][project]]
      on GitLab ([[https://docs.gitlab.com/ce/workflow/forking_workflow.html][fork documentation]])
      - (Instead of forking, maybe use ~git clone~ as [[Installation][shown above]];
        import that as new project on GitLab)
   2. Create project’s files locally
      - ~git clone <the URL of YOUR GitLab project>~
   3. Create or update Org files in cloned directory, add, commit, and
      push (see next slide)
   4. GitLab infrastructure picks up changes and publishes presentations as
      [[https://about.gitlab.com/features/pages/][GitLab Pages]]
      - Takes some minutes
      - Go to Settings → Pages to see the Pages’ address

*** On Git
    - Install Git
      ([[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git][getting started]])
      - The [[https://git-scm.com/book/en/v2][Pro Git book]] is a great source in general
    - Short [[https://try.github.io/levels/1/challenges/1][in-browser Git demo]]
      - Explains Git commands ~add~, ~commit~, ~push~, and more
    - Slightly longer
      [[https://oer.gitlab.io/oer-on-oer-infrastructure/Git-introduction.html][Git introduction as OER]]
      (created with emacs-reveal)

* Some Presentation Features
  :PROPERTIES:
  :CUSTOM_ID: features
  :END:

** Text Slide
   - A list
   - With a sub-list whose items appear
     #+ATTR_REVEAL: :frag (appear)
     - This is /emphasized/
     - This is *bold*
     - This ~looks like code~
     - This is [[color:green][green]]
     - Nothing special

** Some Fragment Styles
   #+ATTR_REVEAL: :frag (gray-out shrink grow highlight-red)
   - Forget
   - Shrink
   - Grow
   - Very important

*** Fragments with Custom Order
    #+ATTR_REVEAL: :frag (appear) :frag_idx (1 4 3 2 1)
    * I’m first.
    * Fourth.
    * Third
    * Second
    * I’m also first.

** Centered Text

   #+ATTR_HTML: :class org-center
   Just some horizontally centered text.  Created by assigning a class
   with ~text-align: center~.

** On Sections
   # As explained in the Org manual, link targets can have different
   # forms.  The first link below points to #features, which has been
   # defined above as CUSTOM_ID.  The second link uses the section
   # header’s text as link target.
   - This slide is part of section [[#features][Some Presentation Features]]
     - We can link to slides, e.g., [[Text Slide][the previous slide]]
       - You can use the browser history to go back
     - Side note: Check source code to see two variants of link
       targets used on this slide
   # The following directive with “appear” lets the next thing
   # appear in its entirety; instead of each list item individually as
   # on the previous slide with “(appear)” in parentheses.
   #+ATTR_REVEAL: :frag appear
   - This slide can also be perceived as its own subsection
     - The [[#another-anchor][next slide]] is on a deeper level of nesting
   - (This list item appears simultaneously with previous bullet point)

*** Another Slide
    :PROPERTIES:
    :CUSTOM_ID: another-anchor
    :END:
    - This slide is on a deeper level of nesting
    - This level of nesting is not shown in the table of contents in
      the slide’s bottom
    - By the way, the headings in the table of contents below are
      hyperlinks
      - And your browser remembers the history, back/forward buttons
        and shortcuts should work
      - Mousewheel and swiping work

** Two Columns: Pro/Con of emacs-reveal
   #+ATTR_REVEAL: :frag appear
   #+BEGIN_leftcol
   Pro
   #+ATTR_REVEAL: :frag (appear)
   - Free/libre open source software
   - Device-independent presentations
     - Also mobile and offline
     - Generated from simple text format
       - Easy to learn
       - Collaboration with diff/merge/git
       - Separation of layout and content
   #+END_leftcol

   #+ATTR_REVEAL: :frag appear
   #+BEGIN_rightcol
   Con
   #+ATTR_REVEAL: :frag (appear)
   - No [[https://en.wikipedia.org/wiki/WYSIWYG][WYSIWYG]]
   - (Need to learn something new)
   #+END_rightcol

* Figures and Audio

** Slide with Figure and Audio
   :PROPERTIES:
   :reveal_extra_attr: data-audio-src="./audio/Tours_-_01_-_Enthusiast.mp3"
   :END:
   - This figure is part of a
     [[https://oer.gitlab.io/OS/OS08-Memory.html][different presentation]]
     {{{reveallicense("./figures/OS/clock-steps.meta","40vh")}}}
   - The song Enthusiast by
     [[http://freemusicarchive.org/music/Tours/][Tours]]
     is licensed under a
     [[http://creativecommons.org/licenses/by/3.0/][Creative Commons Attribution License]]


** Figure with Caption and License
   :PROPERTIES:
   :CUSTOM_ID: figure-with-meta-data
   :END:
   - Display image with meta-data specified in file
     - Simplify sharing of images with source and license
   - Functionality and meta-data format are specific
     to emacs-reveal
     - See next slide for sample file

   {{{revealimg("./figures/3d-man/decision.meta","To share or not to share","30vh")}}}

*** Meta-Data File for Previous Image

#+INCLUDE: ./figures/3d-man/decision.meta src emacs-lisp

** An Image Grid: Computers

{{{revealgrid(42,"./figures/devices/computer.grid",60,4,3,"\"ga1 ga2 ga2 ga3\" \"ga1 ga4 ga5 ga6\" \"ga7 ga8 ga9 ga9\"")}}}

*** Creation of Previous Image Grid
    - Single line in source file, using macro ~revealgrid~
      #+BEGIN_SRC
{{{revealgrid(42,"./figures/devices/computer.grid",60,4,3,"\"ga1 ga2 ga2 ga3\" \"ga1 ga4 ga5 ga6\" \"ga7 ga8 ga9 ga9\"")}}}
      #+END_SRC
      - Arguments explained in [[https://gitlab.com/oer/emacs-reveal/blob/master/config.org][emacs-reveal/config.org]]
      - With ~computer.grid~ as follows
        #+INCLUDE: ./figures/devices/computer.grid src emacs-lisp

** Appearing Items with Audio
   (Audios produced with
   [[https://github.com/marytts/marytts][MaryTTS]],
   converted to Ogg format with [[http://www.audacityteam.org/][Audacity]])

   #+ATTR_REVEAL: :frag (appear) :audio (./audio/1.ogg ./audio/2.ogg ./audio/3.ogg)
   - One
   - Two
   - Three

* Quizzes
** Quiz Plugin
   - Emacs-reveal embeds this
     [[https://gitlab.com/schaepermeier/reveal.js-quiz][quiz plugin]]
     - [[https://schaepermeier.gitlab.io/reveal-quiz-demo/demo.html][Demo of plugin’s author]]
   - In presentations, quizzes support active learning
     - In particular, retrieval practice

** Sample Quiz
#+REVEAL_HTML: <script data-quiz src="./quizzes/sample-quiz.js"></script>

* The End
** Further Reading
   - [[http://orgmode.org/#docs][Manuals and tutorials for Org mode]]
   - [[https://oer.gitlab.io/OS/][Presentations for a course on Operating Systems]]
     demonstrating more features of Org mode (e.g., table of contents
     as agenda, bibliography with citations, PDF export) and
     reveal.js (e.g., notes, animated SVGs)

** Go for it!

   {{{revealimg("./figures/3d-man/steps.meta","The road ahead …")}}}

   [[https://gitlab.com/oer/]]

#+MACRO: copyrightyears 2017, 2018
#+INCLUDE: emacs-reveal/license-template.org

# Local Variables:
# indent-tabs-mode: nil
# End:
